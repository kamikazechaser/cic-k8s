## cic-k8s-dev

### Prerequisites

- [`kubectl`](https://kubernetes.io/docs/tasks/tools/)
- [`helmsman`](https://github.com/Praqma/helmsman)
- [`kubernetes-split-yaml`](https://github.com/mogensen/kubernetes-split-yaml) (optional)

### Deployment

#### Deploy Helm charts

```bash
$ helmsman --apply -f helmsman.yaml
```

#### Deploy manifests

```bash
$ kubectl apply -k manifests
```

#### Manage resources

- Helm charts and its dependant resources should be managed via Helmsman (`helmsman.yaml` file)
- Manifests can be managed either through `kustomize` or the split plain manifests in the `build` folder for finer resource management

To build the plain manifests:

```bash
bash build.sh
```

**Examples**:

```bash
# delete redis
helmsman --destroy --target redis -f helmsman.yaml

# install redis
helmsman --apply --target apply redis -f helmsman.yaml

# destroy bloxberg and all its dependant resources
kubectl delete -R -f build/bloxberg

# install bloxberg and its dependant resources
kubectl apply -R -f build/bloxberg

# delete contract-migration job
# TODO: change to actual name
kubectl apply -f build/grassroots/contract-migration/migration-job.yaml
```
